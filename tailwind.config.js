module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    // colors: { override the tailwind default colors the below colors is the one that trully exist by now. ()
    //   'brand-blue': '#1992d4',
    // },
    extend: {
      colors: {
        'brand-blue': '#1992d4'
      },
      spacing: {
        '72': '18rem'
      }
    },
  },
  variants: {
    // hover, focus, active, responsive
    backgroundColor: ['responsive', 'hover', 'focus', 'active']
  },
  plugins: [],
}

// Generating the full tailwind config with its pre defined value -> npx tailwind init tailwind-full.config.js --full
// Default Minimal Config file

/**
10:15 10-9-2020

- Anything that you put in the theme {
  // code will replace it from the  full tailwind config

  anything that you put inside the extend {
    // will extend the default tailwind property values.
  }
}

*/