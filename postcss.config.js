const purgecss = require('@fullhuman/postcss-purgecss');
const cssnano = require('cssnano');

module.exports = {
    plugins: [
        require('tailwindcss'),
        process.env.NODE_ENV === 'production' ? require('autoprefixer') : null,
        process.env.NODE_ENV === 'production' ?
        cssnano({
            preset: 'default'
        }) : null,
        process.env.NODE_ENV === 'production' ?
        purgecss({
            content: [
                './public/index.html'
            ],
            defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
        }) : null,
    ]
}


/**
11:42 10-9-2020

defaultExtractor: content => content.match(/[A-Za-z-9-_:]+/g)
*/